# Genetic Algorithms with Tensorflow (GATF)

[Gitlab](https://gitlab.com/edgarj/gatf) | [PyPI](https://pypi.org/project/gatf/) | [ReadTheDocs](https://genetic-algorithms-with-tensorflow.readthedocs.io/en/latest/)

Genetic algorithms built with Tensorflow for optimizing objective functions built with Tensorflow

## Installation
Installation should be in a `conda` or `virtualenv` environment. Install dependencies into the environment first then install GATF using pip:

```
pip install gatf
```

__Dependencies:__
- Python >=3.7
- Numpy 1.21
- Tensorflow 2.9

Other versions are expected to work but have not been tested.

## Usage
See the [ReadTheDocs](https://genetic-algorithms-with-tensorflow.readthedocs.io/en/latest/) for documentation and example usage

## Contributors
Contributions are welcome! If you want something new, [fork the repository](https://gitlab.com/edgarj/gatf) and then make a merge request to have it implemented.

## Bugs
[Create an issue on Gitlab](https://gitlab.com/edgarj/gatf/issues) and I'll do my best to address it quickly. 