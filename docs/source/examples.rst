Examples
========

.. toctree::
   :maxdepth: 1

   notebooks/NSGA3
   notebooks/muPlusLambda
   notebooks/ParticleSwarm
