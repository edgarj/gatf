Documentation
=============

``gatf.algorithms``
-------------------

.. automodule:: gatf.algorithms
   :members:
   :undoc-members:
   :show-inheritance:

``gatf.operators``
------------------

.. automodule:: gatf.operators
   :members:
   :undoc-members:
   :show-inheritance:

``gatf.population``
-------------------

.. automodule:: gatf.population
   :members:
   :undoc-members:
   :show-inheritance:

