Installation
============
It is recommended to install GATF in a `conda` or `virtualenv` environment. Dependencies include Tensorflow (2.9.2) and Numpy (1.21.5). Other versions will likely work but have not been tested.

`Install Tensorflow <https://www.tensorflow.org/install>`_ first in a conda environment before installing GATF using pip.

.. code-block:: python

   pip install gatf