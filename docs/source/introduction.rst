Introduction
============

Genetic Algorithms with Tensorflow (GATF) is a very simple library designed for optimizing models created with Tensorflow. The benefit on GATF is that it makes use of Tensorflow datatypes and GPU-compatible operations. This avoids converting between tensor and numpy arrays. It also makes use of expand/reduce tensor operators for pairwise comparisons, rather than nested for-loops, in order to speed up the algorithms.

If your objective function (model) is not written in Tensorflow, then there are better genetic algorithm libraries available that work with lists or Numpy types.