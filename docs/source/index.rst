GATF: Genetic Algorithms with Tensorflow
========================================
`Gitlab <https://gitlab.com/edgarj/gatf>`_ | `PyPI <https://test.pypi.org/project/gatf>`_

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   installation
   gatf
   examples

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
